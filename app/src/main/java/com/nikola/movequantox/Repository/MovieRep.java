package com.nikola.movequantox.Repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Update;
import android.os.AsyncTask;

import com.nikola.movequantox.Room.Movie;
import com.nikola.movequantox.Room.MovieDao;
import com.nikola.movequantox.Room.MovieDatabase;

import java.util.List;

public class MovieRep {
    private MovieDao movieDao;

    private LiveData<List<Movie>> allPopularMovies;
    private LiveData<List<Movie>> allTopRatedMovies;
    private LiveData<List<Movie>> allFavMovies;

    public MovieRep(Application application){
        MovieDatabase db = MovieDatabase.getInstance(application);
        movieDao = db.movieDao();
        allPopularMovies = movieDao.getAllMoviesPop();
        allTopRatedMovies = movieDao.getAllMoviesRat();
        allFavMovies = movieDao.getFav();
    }

    public void insert(Movie movie){
        new InsertMovieAsyncTask(movieDao).execute(movie);
    }

    public void update(Movie movie){
        new UpdateMovieAsyncTask(movieDao).execute(movie);
    }

    public void delete(Movie movie){
        new DeleteMovieAsyncTask(movieDao).execute(movie);
    }

    public void deleteFav(Integer movie){
        new RemoveFavAsyncTask(movieDao).execute(movie);
    }

    public void deleteAllFav(){
        new DeleteAllFavAsyncTask(movieDao).execute();
    }

    public LiveData<List<Movie>> getAllPopularMovies(){
        return allPopularMovies;
    }

    public LiveData<List<Movie>> getAllTopRatedMovies(){
        return allTopRatedMovies;
    }

    public LiveData<List<Movie>> getAllFavMovies(){
        return allFavMovies;
    }


    private static class InsertMovieAsyncTask extends AsyncTask<Movie, Void, Void>{
        private MovieDao movieDao;

        private InsertMovieAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }
        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.insert(movies[0]);
            return null;
        }
    }

    private static class UpdateMovieAsyncTask extends AsyncTask<Movie, Void, Void>{
        private MovieDao movieDao;

        private UpdateMovieAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }
        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.update(movies[0]);
            return null;
        }
    }

    private static class DeleteMovieAsyncTask extends AsyncTask<Movie, Void, Void>{
        private MovieDao movieDao;

        private DeleteMovieAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }
        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.delete(movies[0]);
            return null;
        }
    }

    private static class RemoveFavAsyncTask extends AsyncTask<Integer, Void, Void>{
        private MovieDao movieDao;

        private RemoveFavAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }
        @Override
        protected Void doInBackground(Integer... id) {
            movieDao.removeFavorite(id[0]);
            return null;
        }
    }

    private static class DeleteAllFavAsyncTask extends AsyncTask<Void, Void, Void>{
        private MovieDao movieDao;

        private DeleteAllFavAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }
        @Override
        protected Void doInBackground(Void... Voids) {
            movieDao.removeAllFavorite();
            return null;
        }
    }
}
