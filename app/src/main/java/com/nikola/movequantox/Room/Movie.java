package com.nikola.movequantox.Room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "movie_list")
public class Movie {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private String desc;

    private float rate;

    private String genre;

    private int popularity;

    private boolean fav;

    public Movie(String name, String desc, float rate, String genre, int popularity, boolean fav) {
        this.name = name;
        this.desc = desc;
        this.rate = rate;
        this.genre = genre;
        this.popularity = popularity;
        this.fav = fav;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public float getRate() {
        return rate;
    }

    public String getGenre() {
        return genre;
    }

    public int getPopularity() {
        return popularity;
    }

    public boolean isFav() {
        return fav;
    }

    public void setId(int id) {
        this.id = id;
    }
}
