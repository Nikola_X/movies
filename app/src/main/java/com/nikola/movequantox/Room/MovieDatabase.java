package com.nikola.movequantox.Room;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

@Database(entities = {Movie.class},version = 1,exportSchema = false)
public abstract class MovieDatabase extends RoomDatabase {

    private static MovieDatabase instance;

    public abstract MovieDao movieDao();

    public static synchronized MovieDatabase getInstance(Context context){
        if (instance==null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    MovieDatabase.class, "movie_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallb)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallb = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new popuDbAsyncT(instance).execute();
        }
    };

    private static class popuDbAsyncT extends AsyncTask<Void, Void, Void> {

        private MovieDao movieDao;

        private popuDbAsyncT(MovieDatabase db){
            movieDao = db.movieDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            movieDao.insert(new Movie("test1","desc1",9.5f,"Horror",1000,false));
            movieDao.insert(new Movie("test2","desc2",5.5f,"Comedy",900,false));
            movieDao.insert(new Movie("test3","desc2",7f,"Action",546,true));

            return null;
        }
    }
}
