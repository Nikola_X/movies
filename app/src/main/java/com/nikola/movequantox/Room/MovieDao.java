package com.nikola.movequantox.Room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MovieDao {

    @Insert
    void insert(Movie movie);

    @Update
    void update(Movie movie);

    @Delete
    void delete(Movie movie);

    @Query("UPDATE movie_list SET fav ='true' WHERE  id=:mId")
    void removeFavorite(Integer mId);

    @Query("UPDATE movie_list SET fav = 'false'")
    void removeAllFavorite();

    @Query("SELECT * FROM movie_list ORDER BY popularity DESC")
    LiveData<List<Movie>> getAllMoviesPop();

    @Query("SELECT * FROM movie_list ORDER BY rate DESC")
    LiveData<List<Movie>> getAllMoviesRat();

    @Query("SELECT * FROM movie_list WHERE fav = 'true'")
    LiveData<List<Movie>> getFav();
}
