package com.nikola.movequantox.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.nikola.movequantox.Repository.MovieRep;
import com.nikola.movequantox.Room.Movie;

import java.util.List;

public class MovieViewModel extends AndroidViewModel {

    private MovieRep repository;
    private LiveData<List<Movie>> allPopularMovies;
    private LiveData<List<Movie>> allTopRatedMovies;
    private LiveData<List<Movie>> allFavMovies;
    private Movie movie;

    public MovieViewModel(Application application) {
        super(application);
        repository = new MovieRep(application);
        allPopularMovies = repository.getAllPopularMovies();
        allTopRatedMovies = repository.getAllTopRatedMovies();
        allFavMovies = repository.getAllFavMovies();
    }

    public void insert(Movie movie){
        repository.insert(movie);
    }

    public void update(Movie movie){

    }

    public void delete(Movie movie){

    }

    public void removeFav(Integer id){
        repository.deleteFav(id);
    }

    public void deleteAllFav(){
        repository.deleteAllFav();
    }

    public LiveData<List<Movie>> getAllPopularMovies() {
        return allPopularMovies;
    }

    public LiveData<List<Movie>> getAllTopRatedMovies() {
        return allTopRatedMovies;
    }

    public LiveData<List<Movie>> getAllFavMovies(){
        return allFavMovies;
    }

    public Movie movie(){ return this.movie;}

    public void setMovie(Movie movie){this.movie = movie;}
}
