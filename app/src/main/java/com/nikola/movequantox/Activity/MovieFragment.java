package com.nikola.movequantox.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nikola.movequantox.R;
import com.nikola.movequantox.ViewModel.MovieViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

public class MovieFragment extends AppCompatActivity {
    private TextView title;
    private TextView desc;
    private TextView rating;
    private TextView genre;
    private Button Btn;

    private Intent intent;
    private String mId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_layout);

        final Intent intent = getIntent();
        String mTitle= intent.getStringExtra("title");
        String mDesc= intent.getStringExtra("desc");
        String mGenre= intent.getStringExtra("genre");
        String mRate= intent.getStringExtra("rate");
        mId= intent.getStringExtra("id");

        title = (TextView) findViewById(R.id.title);
        desc = (TextView) findViewById(R.id.desc);
        rating = (TextView) findViewById(R.id.rating);
        genre = (TextView) findViewById(R.id.genre);
        Btn = (Button) findViewById(R.id.btnFav);

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent.putExtra("id", mId);
            }
        });

        title.setText(mTitle);
        desc.setText("Description:\n"+mDesc);
        rating.setText("Genre: "+mGenre);
        genre.setText("Rating: "+mRate);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}
