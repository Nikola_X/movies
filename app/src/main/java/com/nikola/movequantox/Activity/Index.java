package com.nikola.movequantox.Activity;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.nikola.movequantox.Adapter.MoviePageAdapter;
import com.nikola.movequantox.Fragmet.FavoritesFragmet;
import com.nikola.movequantox.Fragmet.PopFragmet;
import com.nikola.movequantox.Fragmet.TopFragmet;
import com.nikola.movequantox.R;
import com.nikola.movequantox.Room.Movie;
import com.nikola.movequantox.ViewModel.MovieViewModel;

import java.util.List;

public class Index extends AppCompatActivity {
    private static final String TAG = "Index";


    private MoviePageAdapter mSPageAdapter;
    private ViewPager mViewPager;
    private MovieViewModel MovieVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        Log.d(TAG, "onCreate: Starting");

        mSPageAdapter = new MoviePageAdapter(getSupportFragmentManager());

        //Set up view pager with section adapter
        mViewPager = (ViewPager) findViewById(R.id.container);
        SetupViewPage(mViewPager);

        TabLayout tab= (TabLayout) findViewById(R.id.tabs);
        tab.setupWithViewPager(mViewPager);
        MovieVM = ViewModelProviders.of(this).get(MovieViewModel.class);

    }

    private void SetupViewPage(ViewPager VP){
        MoviePageAdapter adapter = new MoviePageAdapter(getSupportFragmentManager());

        adapter.addFragment(new TopFragmet(),"Top Rated");
        adapter.addFragment(new PopFragmet(),"Popular");
        adapter.addFragment(new FavoritesFragmet(),"Favorite");

        VP.setAdapter(adapter);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                MovieVM.removeFav(Integer.valueOf(data.getStringExtra("id")));
            }
        }
    }

}
