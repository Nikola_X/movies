package com.nikola.movequantox.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nikola.movequantox.Activity.MovieFragment;
import com.nikola.movequantox.R;
import com.nikola.movequantox.Room.Movie;
import com.nikola.movequantox.ViewModel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.mViewHolder>{
    private List<Movie> movies =new ArrayList<>();
    private static final String TAG = "MovieAdapter";

    public class mViewHolder extends RecyclerView.ViewHolder{
        private TextView mTitle;
        private TextView mDesc;
        private TextView mGrn;
        private TextView mRating;
        private RelativeLayout pos;

        public mViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitle =(TextView) itemView.findViewById(R.id.title);
            mDesc =(TextView) itemView.findViewById(R.id.desc);
            mGrn =(TextView) itemView.findViewById(R.id.genre);
            mRating =(TextView) itemView.findViewById(R.id.rating);
            pos = (RelativeLayout) itemView.findViewById(R.id.rel);
        }
    }

    @NonNull
    @Override
    public MovieAdapter.mViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_model,parent,false);
        return new mViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieAdapter.mViewHolder holder, int position) {
        final Movie currentMovie = movies.get(position);
        holder.mTitle.setText(currentMovie.getName());
        holder.mDesc.setText(currentMovie.getDesc());
        holder.mGrn.setText(currentMovie.getGenre());
        holder.mRating.setText(String.valueOf("Rating: "+currentMovie.getRate()));
        holder.pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MovieViewModel model = ViewModelProviders.of((FragmentActivity) v.getContext()).get(MovieViewModel.class);
                model.setMovie(currentMovie);

                Intent intent = new Intent((FragmentActivity) v.getContext(), MovieFragment.class);
                intent.putExtra("title",currentMovie.getName());
                intent.putExtra("desc",currentMovie.getDesc());
                intent.putExtra("rate",String.valueOf(currentMovie.getRate()));
                intent.putExtra("genre",currentMovie.getGenre());
                intent.putExtra("id",currentMovie.getId());
                v.getContext().startActivity(intent);
            }
        });
    }


    public void setMovies(List<Movie> movies){
        this.movies = movies;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
