package com.nikola.movequantox.Fragmet;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nikola.movequantox.Adapter.MovieAdapter;
import com.nikola.movequantox.R;
import com.nikola.movequantox.Room.Movie;
import com.nikola.movequantox.ViewModel.MovieViewModel;

import java.util.List;

public class PopFragmet extends Fragment {
    private static final String TAG = "FavoritesFragmet";

    private RecyclerView mRView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLManager;
    private MovieViewModel MovieVM;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popular,container,false);
        mRView = (RecyclerView) view.findViewById(R.id.rec);
        mRView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRView.setHasFixedSize(true);

        final MovieAdapter mAdapter = new MovieAdapter();
        mRView.setAdapter(mAdapter);

        MovieVM = ViewModelProviders.of(getActivity()).get(MovieViewModel.class);
        MovieVM.getAllPopularMovies()
                .observe(this, new Observer<List<Movie>>() {
                    @Override
                    public void onChanged(@Nullable List<Movie> movies) {
                        mAdapter.setMovies(movies);
                    }
                });

        return view;
    }
}
